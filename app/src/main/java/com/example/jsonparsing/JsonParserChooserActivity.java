package com.example.jsonparsing;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class JsonParserChooserActivity extends ActionBarActivity implements View.OnClickListener {

    private Button buttonParseFromFile, buttonParseFromServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_parser_chooser);

        buttonParseFromFile = (Button) findViewById(R.id.button_parse_from_file);
        buttonParseFromServer = (Button) findViewById(R.id.button_parse_from_server);

        buttonParseFromFile.setOnClickListener(this);
        buttonParseFromServer.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_json_parser_chooser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View clickedButton) {
        if (clickedButton.getId() == buttonParseFromFile.getId()) {
            Intent intent = new Intent(JsonParserChooserActivity.this, JsonParsingActivity.class);
            startActivity(intent);
        } else if (clickedButton.getId() == buttonParseFromServer.getId()) {
            Intent intent = new Intent(JsonParserChooserActivity.this, ServerJsonParsingActivity.class);
            startActivity(intent);
        }
    }
}
