package com.example.jsonparsing.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for holding student's name
 * i.e first name and last name
 */
public class Name {

    //SerializedName with "key" in the bracket is needed for GSON to work.

    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
