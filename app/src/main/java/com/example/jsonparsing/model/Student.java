package com.example.jsonparsing.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Class representing student, holds general informations like
 * name, address, level & institute of study
 */
public class Student {

    //SerializedName with "key" in the bracket is needed for GSON to work.

    @SerializedName("name")
    private Name name;
    @SerializedName("address")
    private String address;
    @SerializedName("study")
    private List<Study> studyList;

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Study> getStudyList() {
        return studyList;
    }

    public void setStudyList(List<Study> studyList) {
        this.studyList = studyList;
    }
}
