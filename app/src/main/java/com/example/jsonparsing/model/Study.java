package com.example.jsonparsing.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class to hold the study details of the student.
 * It gives institute and level data.
 */
public class Study {

    //SerializedName with "key" in the bracket is needed for GSON to work.

    @SerializedName("institute")
    private String institute;
    @SerializedName("level")
    private String level;

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
