package com.example.jsonparsing;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jsonparsing.model.Student;
import com.example.jsonparsing.model.Study;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Activity which opens a local file containing Json data and show the parsed data as result.
 */
public class JsonParsingActivity extends Activity implements View.OnClickListener {

    private TextView textViewInput, textViewOutput;
    private Button buttonParse;

    // a "global" variable to store the extracted json data from the JsonSample.json file
    private String inputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_parsing);

        textViewInput = (TextView) findViewById(R.id.textview_input);
        textViewOutput = (TextView) findViewById(R.id.textview_output);
        buttonParse = (Button) findViewById(R.id.button_parse);

        buttonParse.setOnClickListener(this);

        // Fetch data from the JsonSample.json
        inputData = fetchInputData();
        textViewInput.setText(inputData);
    }

    /**
     * Read the "JsonSample.json" we created in assets folder and return
     * the result as String
     *
     * @return json data as String
     */
    private String fetchInputData() {
        String json;
        try {
            InputStream inputStream = getAssets().open("JsonSample.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            json = null;
        }
        return json;
    }

    @Override
    public void onClick(View v) {
        parseJsonData();
    }

    /**
     * Method for parsing the json data
     */
    private void parseJsonData() {
        if (TextUtils.isEmpty(inputData)) {
            textViewOutput.setText("No data");
        } else {
            try {
                // Make a new Gson object
                Gson gson = new Gson();
                // use that Gson object to get the outer main class(Student class in our case).
                // first parameter is the json result and second is target class(Student.class)
                Student studentObject = gson.fromJson(inputData, Student.class);

                String studyCombined = "";

                // getting the study list
                for (int i = 0; i < studentObject.getStudyList().size(); i++) {
                    Study studyObject = studentObject.getStudyList().get(i);
                    studyCombined += studyObject.getLevel()
                            + " from "
                            + studyObject.getInstitute()
                            + "\n";
                }

                // foreach loop can be used as well
            /*for(Study studyObject : studentObject.getStudyList()) {
                studyCombined += studyObject.getLevel()
                        + " from "
                        + studyObject.getInstitute()
                        + "\n";
            }*/

                // Lets concatenate all the extracted values and make a result to display
                String resultToDisplay = studentObject.getName().getFirstName()
                        + " "
                        + studentObject.getName().getLastName()
                        + "\n"
                        + studentObject.getAddress()
                        + "\n"
                        + studyCombined;

                // and set that result to output TextView
                textViewOutput.setText(resultToDisplay);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                textViewOutput.setText("Json parsing exception");
            }
        }
    }
}
