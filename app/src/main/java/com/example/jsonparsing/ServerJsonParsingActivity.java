package com.example.jsonparsing;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Activity which fetches data from server, parses it and show the parsed data
 */
public class ServerJsonParsingActivity extends Activity implements View.OnClickListener {
    private TextView textViewInput, textViewOutput;
    private Button buttonParse;

    // I have uploaded our json data in the www.json-generator.com and generated the following link
    // for getting the json data. you can upload your own json data and generate a seperate link
    // for it.
    private final String jsonUrl = "http://www.json-generator.com/api/json/get/cqKwksGEfC?indent=2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_parsing);

        textViewInput = (TextView) findViewById(R.id.textview_input);
        textViewOutput = (TextView) findViewById(R.id.textview_output);
        buttonParse = (Button) findViewById(R.id.button_parse);

        buttonParse.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // at first obtain the json data from the server.
        // set that data in the input TextView
        // parse the json and set the result in the output/display TextView

        // it is a better approach to check for internet connectivity before hitting the server
        if (Utils.isNetworkConnected(this)) {
            new DataParserAsyncTask(this).execute(jsonUrl);
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This method is called by the AsyncTask when data fetching(json data) is completed.
     *
     * @param responseFromServer asyncTask sends the response as the parameter
     */
    public void parseJsonResponse(String responseFromServer) {
        if (TextUtils.isEmpty(responseFromServer)) {
            textViewOutput.setText("No data");
        } else {
            // setting the actual Json response to upper TextView
            textViewInput.setText(responseFromServer);

            try {
                JSONObject mainJsonObject = new JSONObject(responseFromServer);

                // Extract name object from the main json object
                JSONObject nameJsonObject = mainJsonObject.getJSONObject("name");
                // get the first name from the name JsonObject
                String firstName = nameJsonObject.getString("first_name");
                // get the last name from the name JsonObject
                String lastName = nameJsonObject.getString("last_name");

                // get the address from the main JsonObject
                String address = mainJsonObject.getString("address");

                // get the study JsonArray from the main JsonObject
                JSONArray studyJsonArray = mainJsonObject.getJSONArray("study");
                // get the length of the array for looping and getting the data
                int lengthOfStudyJsonArray = studyJsonArray.length();
                // loop till the length of array
                String[] study = new String[lengthOfStudyJsonArray];
                for (int i = 0; i < lengthOfStudyJsonArray; i++) {
                    // get the study JsonObject from the study JsonArray
                    JSONObject studyJsonObject = studyJsonArray.getJSONObject(i);
                    // get the institute from the study JsonObject
                    String institute = studyJsonObject.getString("institute");
                    // get the level from the study JsonObject
                    String level = studyJsonObject.getString("level");
                    // concatenate the results and store in "study" array
                    study[i] = level + " from " + institute;
                }

                // String variable to hold all the "study"s
                String studyCombined = "";
                for (int i = 0; i < lengthOfStudyJsonArray; i++) {
                    studyCombined += study[i] + "\n";
                }
                // Lets concatenate all the extracted values and make a result to display
                String resultToDisplay = firstName + " " + lastName
                        + "\n"
                        + address
                        + "\n"
                        + studyCombined;

                // and set that result to output TextView
                textViewOutput.setText(resultToDisplay);
            } catch (JSONException e) {
                e.printStackTrace();
                // if some JSONException occurs lets show it in the TextView
                textViewOutput.setText("Json exception occurred");
            }
        }
    }
}
